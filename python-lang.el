;; Python Mode
(add-hook 'python-mode-hook '(lambda ()
  (setq python-indent 2)
  (setq whitespace-line-column 80
        whitespace-style '(face tabs lines-tail trailing))  ;; removed 'empty'
  (whitespace-mode t)
  (add-hook 'local-write-file-hooks 'whitespace-cleanup-on-save)
  ))
