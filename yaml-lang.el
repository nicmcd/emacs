;; Yaml Mode
(add-hook 'yaml-mode-hook '(lambda ()
  (define-key yaml-mode-map "\C-m" 'newline-and-indent)))
  ; no indent ???
  ;(setq whitespace-line-column 80
  ;      whitespace-style '(face empty tabs lines-tail trailing))
  ;(whitespace-mode t)
  ;(add-hook 'local-write-file-hooks 'whitespace-cleanup-on-save)
  ;))

(setq auto-mode-alist (cons '("\\.yml\\'" . yaml-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.yaml\\'" . yaml-mode) auto-mode-alist))
