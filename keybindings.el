(global-set-key "\C-u" 'undo)
(global-set-key "\M-s" 'save-buffer)
(global-set-key "\M-o" 'find-file)
(global-set-key "\C-v" 'yank)
(global-set-key "\M-u" 'upcase-word)
(global-set-key "\M-l" 'downcase-word)

(global-set-key "\M-0" 'delete-window)
(global-set-key "\M-1" 'delete-other-windows)
(global-set-key "\M-2" 'split-window-vertically)
(global-set-key "\M-3" 'split-window-horizontally)
(global-set-key "\M-g" 'goto-line)

(global-set-key (kbd "C-S-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-S-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-S-<up>") 'enlarge-window)
(global-set-key (kbd "C-S-<down>") 'shrink-window)
