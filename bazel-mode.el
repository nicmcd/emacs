(defgroup bazel nil
  "Major mode for editing Bazel files."
  :prefix "bazel-"
  :group 'languages)

;;;###autoload
(define-derived-mode bazel-mode python-mode "Bazel"
  "Major mode for editing Bazel files."
  :group 'bazel

  (setq-local comment-use-syntax t)
  (setq-local comment-start "#")
  (setq-local comment-end "")
  (setq-local indent-tabs-mode nil))

(provide 'bazel-mode)
