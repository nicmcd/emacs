#!/usr/bin/env python3

import argparse
import glob
import os
import subprocess


def main(args):
  # create a map between source and destinations
  files = {}
  base = os.path.expanduser('~/.emacs.d')
  for src in glob.glob('*.el'):
    if src == 'init.el':
      files[src] = os.path.join(base, src)
    else:
      files[src] = os.path.join(base, 'lisp', src)

  # run checks before deleting anything
  assert 'init.el' in files.keys(), 'init.el must exist!'

  # clean old installation
  subprocess.check_call('rm -rf ~/.emacs.d', shell=True)

  # create directories
  os.makedirs(os.path.join(base, 'lisp'))

  # install new files
  for src in sorted(files.keys()):
    print('{0} -> {1}'.format(src, files[src]))
    install = 'cp {0} {1}'.format(src, files[src])
    subprocess.check_call(install, shell=True)

  # byte compile all files
  #compile = ('emacs -batch -f batch-byte-compile '
  #           '~/.emacs.d/lisp/*.el ~/.emacs.d/init.el')
  #subprocess.check_call(compile, shell=True)

  return 0


if __name__ == '__main__':
  ap = argparse.ArgumentParser()
  args = ap.parse_args()
  main(args)
