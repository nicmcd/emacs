;; CMake Mode
(add-hook 'cmake-mode-hook '(lambda ()
  (setq cmake-indent-width 2
	cmake-auto-indent-on-newline nil
        cmake-auto-indent-on-braces nil
	cmake-auto-indent-on-semi nil
	tab-width 2
        indent-tabs-mode nil)
  (setq whitespace-line-column 80
        whitespace-style '(face empty tabs lines-tail trailing))
  (whitespace-mode t)
  (add-hook 'local-write-file-hooks 'whitespace-cleanup-on-save)
  ))

(setq auto-mode-alist (cons '("\\CMakeLists.txt\\'" . cmake-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\CmakeLists.txt\\'" . cmake-mode) auto-mode-alist))
